#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

from numpy.random import choice, poisson, dirichlet

from scipy.sparse import csr_matrix

from time import time

from plsa import PLSA

# ================================================================================
# Constants.
# ================================================================================

n_topics = 5
n_words = 500
n_documents = 1000
n_avg_doc_len = 100
n_iterations = 1000
n_jobs = -1

topic_assignment_known = False

# ================================================================================
# Generating training/test documents.
# ================================================================================

print
print 'Generating a corpus'
print '-------------------'
print '#documents:', n_documents
print '#words:    ', n_words
print '#topics:   ', n_topics

# To make sure everything works out the same every time.
np.random.seed(42)

# Sample the topic distribution for each document.
p_z_d = dirichlet([1. / n_topics] * n_topics, n_documents)

# Sample the word distribution for each topic.
p_w_z = dirichlet([1. / n_words] * n_words, n_topics)

# Sample the actual "content" of the documents.
n_w_d = np.zeros((n_documents, n_words), dtype='int32')

for d in range(n_documents):
    # Sample a topic for each word in the document.
    for z, c in enumerate(np.bincount(choice(range(n_topics), size=poisson(n_avg_doc_len), p=p_z_d[d]), minlength=n_topics)):
        # z - topic ID
        # c - the number of words from topic z in document d.
        n_w_d[d] += np.random.multinomial(c, p_w_z[z])

# Get rid of empty columns (keep the words seen at least once).
# This may still leave empty columns in train and/or test matrices
# after splitting documents, but PLSA can handle that.
sampled_word_indices = np.where(n_w_d.sum(axis=0))[0]

# Renormalize word distribution (should not change that much).
p_w_z = p_w_z[:, sampled_word_indices]
p_w_z /= p_w_z.sum(axis=1, keepdims=True)

n_words = sampled_word_indices.shape[0]

train_dense_n_w_d = np.ascontiguousarray(n_w_d[:int(0.8 * n_documents), sampled_word_indices])
test_dense_n_w_d = np.ascontiguousarray(n_w_d[int(0.8 * n_documents):, sampled_word_indices])

del n_w_d

train_sparse_n_w_d = csr_matrix(train_dense_n_w_d)
test_sparse_n_w_d = csr_matrix(test_dense_n_w_d)

# ================================================================================
# Simulate training on dense document-term matrix.
# ================================================================================

print
print 'Learning PLSA on dense document-term matrix.'
print '--------------------------------------------'

np.random.seed(42)
dense_model = PLSA()
# Here, `heldout=test_dense_n_w_d` could have been used to see
# how the likelihood changes on the test set.
start = time()
dense_model.learn(train_dense_n_w_d, n_topics=n_topics, n_iterations=n_iterations, verbose=1, n_jobs=n_jobs)
print 'Elapsed time: %.2fs' % (time() - start)

# ================================================================================
# Simulate training on dense document-term matrix.
# ================================================================================

print
print 'Learning PLSA on sparse document-term matrix.'
print '---------------------------------------------'

np.random.seed(42)
sparse_model = PLSA()
# Here, `heldout=test_dense_n_w_d` could have been used to see
# how the likelihood changes on the test set.
start = time()
sparse_model.learn(train_sparse_n_w_d, n_topics=n_topics, n_iterations=n_iterations, verbose=1, n_jobs=n_jobs)
print 'Elapsed time: %.2fs' % (time() - start)

# Sanity check.
assert np.allclose(dense_model.p_z_d, sparse_model.p_z_d), "Something's wrong! The models should yield the same parameters!"
assert np.allclose(dense_model.p_w_z, sparse_model.p_w_z), "Something's wrong! The models should yield the same parameters!"

# ================================================================================
# Simulate inference of topics on unseen documents.
# -------------------------------------------------
# Try to find the mapping between true and estimated topics. Sometimes the
# generated data makes the model not identifiable (rotation is an inherent
# problem, which should be theoretically resolved by the following code, if
# the data produce distinct (enough) word distributions). 
# ================================================================================

np.set_printoptions(threshold=np.nan, linewidth=np.nan, suppress=True, precision=2)

X = np.dot(dense_model.p_z_d.T, p_z_d[:int(0.8 * n_documents)])

first_options_idx = X.argmax(axis=1)
first_options_val = np.amax(X, axis=1)

X[np.arange(n_topics), X.argmax(axis=1)] = -1

second_options_idx = X.argmax(axis=1)
second_options_val = np.amax(X, axis=1)

print
print 'Finding possible topic matching:'
print '--------------------------------' 
for i in zip(np.arange(n_topics), first_options_idx, first_options_val, second_options_idx, second_options_val):
    print '%2d -> %2d (weight: %8.2f) or maybe %d (weight: %8.2f)' % i    
print
print 'Checkout the suggested topic matching:'
print sparse_model.p_z_d[:10]
print
print p_z_d[:10]

# Inferring the topic distributions.
print
print 'Inferring topic distribution for unseen documents.'
print '--------------------------------------------------'
start = time()
inferred_test_dense_p_z_d = dense_model.infer(test_dense_n_w_d, n_iterations=n_iterations, n_jobs=n_jobs)
print 'Elapsed time (dense): %.2fs' % (time() - start)
start = time()
inferred_test_sparse_p_z_d = sparse_model.infer(test_sparse_n_w_d, n_iterations=n_iterations, n_jobs=n_jobs)
print 'Elapsed time (sparse): %.2fs' % (time() - start)

# Sanity check.
assert np.allclose(inferred_test_dense_p_z_d, inferred_test_sparse_p_z_d), \
       "Something's wrong! The inferred topic distributions should be the same! Perhaps the models' parameters have not converged during training."

# If the topic assignments are known they can be used to reorder
# inferred topic distributions for unseen documents
if topic_assignment_known:
    # The eyeballed topic assignments.
    z_indices = np.array([], dtype=np.intc)

    # Extract topic distribution of test documents.
    test_p_z_d = p_z_d[int(0.8 * n_documents):]

    print
    print "Printing true and inferred topic distributions for the 1st 20 test documents."
    print "-----------------------------------------------------------------------------"
    print test_p_z_d[:20]
    print
    print inferred_test_dense_p_z_d[:20, z_indices]

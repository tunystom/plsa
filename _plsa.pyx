# cython: cdivision=True
# cython: boundscheck=False
# cython: wraparound=False

from libc.math cimport log, pow
from libc.string cimport memset

cimport numpy as np
import numpy as np

ctypedef np.npy_int32   INT      # Using 32-bit integer for word counts should be more than enough.
ctypedef np.npy_float64 DOUBLE   # Calling it double, but it can be easily changed to single precision.

cpdef sparse_plsa_update_parameters(INT start, INT end, INT[::1] indptr, INT[::1] indices, INT[::1] counts,
                                    DOUBLE[:, ::1] p_z_d, DOUBLE[:, ::1] p_w_z, DOUBLE[:, ::1] p_z_w_d,
                                    DOUBLE[:, ::1] out_p_w_z, DOUBLE[::1] n_w, DOUBLE beta):
    '''
    (Tempered) Expectation-Maximization Algorithm for pLSA
    ------------------------------------------------------
    Computes a single iteration of EM algorithm for reestimation of pLSA model parameters
    over the given range of documents.

    The corresponding input topic distributions will be updated (i.e. p_z_d for given range
    of documents), but since word distribution per topic is shared by all documents, the update
    of this distribution must be made out of this function by summing up `out_p_w_z` arrays
    and renormalizing rows.
    
    This implementation is for sparse document-term matrices, hence the name. Similar method
    with dense_ prefix in its name should be used for dense matrices.

    Parameters
    ----------
    start: integer
      The index of the 1st document to process.

    end: integer
      The index (exclusive) of the last document to process.

    indptr: array of integers
      The index pointer array from the sparse document-term count matrix.

    indices: array of integers
      The indices array from the sparse document-term count matrix.

    counts: array of integers
      The data array from the sparse document-term count matrix.

    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for each document.

    p_w_z: array of doubles, shape = (n_topics, n_words)
      The word distribution for each topic.

    p_z_w_d: array of doubles, shape = (n_words, n_topics)
      The temporary array for posterior topic distribution given
      a word and a document.

    out_p_w_z: array of doubles, shape = (n_topics, n_words)
      The output array for unnormalized word distribution for each topic
      computed from the documents start up to end.
    
    n_w: array of doubles, shape = (n_documents,)
      The length of the documents. Used for faster normalization of P(z|d),
      hence using double to avoid unnecessary conversion.
      
    beta: double
      The 'inverse computational temperature' used for tempered expectation
      maximization algorithm. If set to 1, it is an ordinary EM algorithm.
    '''
    cdef INT d, w, wi, z
    cdef DOUBLE p_w_d
    cdef INT n_words = p_w_z.shape[1]
    cdef INT n_topics = p_z_d.shape[1]
    # Releasing GIL to allow multi-threading.
    with nogil:
        # Zero the output matrix for partial sum of unnormalized P(w|z).
        memset(&out_p_w_z[0, 0], 0, n_words * n_topics * sizeof(DOUBLE))
        # Reestimate P(z|d) for documents in range [start, end) and
        # save the shared counts for reestimation of P(w|z).
        for d in range(start, end):
            # Compute P(z|w,d) for current d.
            for w in range(n_words):
                # Contains (tempered) P(w|d) once the next for-loop finishes.
                p_w_d = 0.0
                for z in range(n_topics):
                    p_z_w_d[w, z] = pow(p_z_d[d, z] * p_w_z[z, w], beta)
                    p_w_d += p_z_w_d[w, z]
                # Normalize to get posterior topic distribution P(z|w,d).
                if p_w_d != 0.0:
                    for z in range(n_topics):
                        p_z_w_d[w, z] /= p_w_d
            # Reestimate P(z|d) for current d.
            for z in range(n_topics):
                p_z_d[d, z] = 0.0
                for wi in range(indptr[d], indptr[d + 1]):
                    w = indices[wi]
                    p_z_w_d[w, z] *= counts[wi]
                    p_z_d[d, z] += p_z_w_d[w, z]
                    # Reestimation of P(w|z) must be made externally.
                    out_p_w_z[z, w] += p_z_w_d[w, z]
            # Normalize p_z_d to get P(z|d).
            for z in range(n_topics):
                p_z_d[d, z] /= n_w[d]

                    
cpdef sparse_plsa_folding_in(INT start, INT end, INT[::1] indptr, INT[::1] indices, INT[::1] counts,
                             DOUBLE[:, ::1] p_z_d, DOUBLE[:, ::1] p_w_z, DOUBLE[:, ::1] p_z_w_d,
                             DOUBLE[::1] n_w, DOUBLE beta, DOUBLE theta, DOUBLE improvement, INT n_iterations):
    '''
    Folding-in using (Tempered) Expectation-Maximization Algorithm for pLSA
    -----------------------------------------------------------------------
    Estimates topic distributions for documents not seen during learning of the model.
    
    The corresponding input topic distributions will be updated (i.e. p_z_d for given range
    of documents), the word distribution per topic is hold fixed.
    
    This implementation is for sparse document-term matrices, hence the name. Similar method
    with dense_ prefix in its name should be used for dense matrices.

    Parameters
    ----------
    start: integer
      The index of the 1st document to process.

    end: integer
      The index (exclusive) of the last document to process.

    indptr: array of integers
      The index pointer array from the sparse document-term count matrix.

    indices: array of integers
      The indices array from the sparse document-term count matrix.

    counts: array of integers
      The data array from the sparse document-term count matrix.

    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for each document.

    p_w_z: array of doubles, shape = (n_topics, n_words)
      The word distribution for each topic.

    p_z_w_d: array of doubles, shape = (n_words, n_topics)
      The temporary array for posterior topic distribution given
      a word and a document.

    n_w: array of doubles, shape = (n_documents,)
      The length of the documents. Used for faster normalization of P(z|d),
      hence using double to avoid unnecessary conversion.
      
    beta: double
      The 'inverse computational temperature' used for Tempered Expectation
      Maximization algorithm. If set to 1.0, regula EM algorithm is used.
    
    theta: double
      The (multiplicative) 'heating' rate for Tempered-EM algorithm.
      
    improvement: double
      The minimum improvement in log-likelihood to keep reestimating
      the topic distributions.
    
    n_iterations: integer
      The (total, not per-beta) number of iterations of Tempered-EM algorithm.
    '''
    cdef INT i, d, w, wi, z
    cdef DOUBLE p_w_d, prev_log_likelihood, curr_log_likelihood
    cdef INT n_words = p_w_z.shape[1]
    cdef INT n_topics = p_z_d.shape[1]
    cdef bint heated_and_not_improved = False
    # Releasing GIL to allow multi-threading.
    with nogil:
        # Compute the initial log-likelihood for the documents.
        prev_log_likelihood = _sparse_plsa_log_likelihood(start, end, indptr, indices, counts, p_z_d, p_w_z)
        # Run (Tempered) Expecation Maximization.
        for i in range(n_iterations):
            # Reestimate P(z|d) for documents in range [start, end).
            for d in range(start, end):
                # Compute (tempered) P(z|w,d) for current d.
                for w in range(n_words):
                    # Contains (tempered) P(w|d) once the next for-loop finishes.
                    p_w_d = 0.0
                    for z in range(n_topics):
                        p_z_w_d[w, z] = pow(p_z_d[d, z] * p_w_z[z, w], beta)
                        p_w_d += p_z_w_d[w, z]
                    # Normalize to get posterior topic distribution P(z|w,d).
                    if p_w_d != 0.0:
                        for z in range(n_topics):
                            p_z_w_d[w, z] /= p_w_d
                # Reestimate P(z|d) for current d.
                for z in range(n_topics):
                    p_z_d[d, z] = 0.0
                    for wi in range(indptr[d], indptr[d + 1]):
                        p_z_d[d, z] += counts[wi] * p_z_w_d[indices[wi], z]
                # Normalize p_z_d to get P(z|d).
                for z in range(n_topics):
                    p_z_d[d, z] /= n_w[d]
            # Compute the log-likelihood of the documents after reestimation of P(z|d).
            curr_log_likelihood = _sparse_plsa_log_likelihood(start, end, indptr, indices, counts, p_z_d, p_w_z)
            # Shoud we heat up or stop?
            if curr_log_likelihood - prev_log_likelihood < improvement:
                # If no heating is wanted (using regular EM), or heating has
                # not led to significant improvement then stop.
                if theta == 1.0 or heated_and_not_improved:
                    break
                else:
                    beta = theta * beta
                    # Mark that the estimation has just been heated :).
                    heated_and_not_improved = True
            else:
                # Mark the improvement.
                heated_and_not_improved = False
                    


cpdef sparse_plsa_log_likelihood(INT start, INT end, INT[::1] indptr, INT[::1] indices, INT[::1] counts,
                                 DOUBLE[:, ::1] p_z_d, DOUBLE[:, ::1] p_w_z):
    '''
    Log-likelihood for pLSA
    -----------------------
    Computes log-likelihood of the data (sparse matrix: (indptr, indices, counts) given
    the pLSA model parametrized by p_z_d and p_w_z, i.e. P(z|d) and P(w|z), respectively.
    
    Note that the returned value is not the real log-likelihood but it is off from it
    by a constant, which depends only on the data, so who cares ;).
    
    This implementation is for sparse document-term matrices, hence the name. Similar method
    with dense_ prefix in its name should be used for dense matrices.

    Parameters
    ----------
    start: integer
      The index of the 1st document to process.

    end: integer
      The index (exclusive) of the last document to process.

    indptr: array of integers
      The index pointer array from the sparse document-term count matrix.

    indices: array of integers
      The indices array from the sparse document-term count matrix.

    counts: array of integers
      The data array from the sparse document-term count matrix.

    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for each document.

    p_w_z: array of doubles, shape = (n_topics, n_words)
      The word distribution for each topic.
    '''
    cdef DOUBLE likelihood
    with nogil:
        likelihood = _sparse_plsa_log_likelihood(start, end, indptr, indices, counts, p_z_d, p_w_z)
    return likelihood


cdef inline DOUBLE _sparse_plsa_log_likelihood(INT start, INT end, INT[::1] indptr, INT[::1] indices, INT[::1] counts,
                                               DOUBLE[:, ::1] p_z_d, DOUBLE[:, ::1] p_w_z) nogil:
    '''
    The guts of 'sparse_plsa_log_likelihood', see above.
    '''
    cdef INT d, w, wi, z, n_topics = p_z_d.shape[1]
    cdef DOUBLE p_w_d, likelihood = 0.0
    # Compute the log-likelihood for documents in range [start, end).
    for d in range(start, end):
        for wi in range(indptr[d], indptr[d + 1]):
            w = indices[wi]
            p_w_d = 0.0
            for z in range(n_topics):
                p_w_d += p_w_z[z, w] * p_z_d[d, z]
            if p_w_d != 0.0:
                likelihood += counts[wi] * log(p_w_d)
    return likelihood


cpdef dense_plsa_update_parameters(INT start, INT end, INT[:, ::1] n_w_d,
                                   DOUBLE[:, ::1] p_z_d, DOUBLE[:, ::1] p_w_z,
                                   DOUBLE[:, ::1] p_z_w_d, DOUBLE[:, ::1] out_p_w_z,
                                   DOUBLE[::1] n_w, DOUBLE beta):
    '''
    Expectation-Maximization Algorithm for pLSA
    -------------------------------------------
    Computes a single iteration of EM algorithm for reestimation of pLSA model parameters
    over the given range of documents.

    The corresponding input topic distributions will be updated (i.e. p_z_d for given range
    of documents), but since word distribution per topic is shared by all documents, the update
    of this distribution must be made out of this function by summing up `out_p_w_z` arrays
    and renormalizing rows.
    
    This implementation is for dense document-term matrices, hence the name. Similar method
    with sparse_ prefix in its name should be used for sparse matrices.

    Parameters
    ----------
    start: integer
      The index of the 1st document to process.

    end: integer
      The index (exclusive) of the last document to process.

    n_w_d: array of doublese, shape = (n_documents, n_words)
      The the dense document-term count matrix.

    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for each document.

    p_w_z: array of doubles, shape = (n_topics, n_words)
      The word distribution for each topic.

    p_z_w_d: array of doubles, shape = (n_words, n_topics)
      The temporary array for posterior topic distribution given
      a word and a document.

    out_p_w_z: array of doubles, shape = (n_topics, n_words)
      The output array for unnormalized word distribution for each topic
      computed from the documents start up to end.
    
    n_w: array of doubles, shape = (n_documents,)
      The length of the documents. Used for faster normalization of P(z|d),
      hence using double to avoid unnecessary conversion.
      
    beta: double
      The 'inverse computational temperature' used for tempered expectation
      maximization algorithm. If set to 1, it is an ordinary EM algorithm.
    '''
    cdef INT d, w, z
    cdef DOUBLE p_w_d
    cdef INT n_words = p_w_z.shape[1]
    cdef INT n_topics = p_z_d.shape[1]
    # Releasing GIL to allow multi-threading.
    with nogil:
        # Zero the output matrix for partial sum of unnormalized P(w|z).
        memset(&out_p_w_z[0, 0], 0, n_words * n_topics * sizeof(DOUBLE))
        # Reestimate P(z|d) for documents in range [start, end) and
        # save the shared counts for reestimation of P(w|z).
        for d in range(start, end):
            # Compute P(z|w,d) for current d.
            for w in range(n_words):
                # Contains (tempered) P(w|d) once the next for-loop finishes.
                p_w_d = 0.0
                for z in range(n_topics):
                    p_z_w_d[w, z] = pow(p_z_d[d, z] * p_w_z[z, w], beta)
                    p_w_d += p_z_w_d[w, z]
                # Normalize to get posterior topic distribution P(z|w,d).
                if p_w_d != 0.0:
                    for z in range(n_topics):
                        p_z_w_d[w, z] /= p_w_d
            # Reestimate P(z|d) for current d.
            for z in range(n_topics):
                p_z_d[d, z] = 0.0
                for w in range(n_words):
                    p_z_w_d[w, z] *= n_w_d[d, w]
                    p_z_d[d, z] += p_z_w_d[w, z]
                    # Reestimation of P(w|z) must be made externally.
                    out_p_w_z[z, w] += p_z_w_d[w, z]
            # Normalize p_z_d to get P(z|d).
            for z in range(n_topics):
                p_z_d[d, z] /= n_w[d]


cpdef dense_plsa_folding_in(INT start, INT end, INT[:, ::1] n_w_d, DOUBLE[:, ::1] p_z_d, DOUBLE[:, ::1] p_w_z,
                            DOUBLE[:, ::1] p_z_w_d, DOUBLE[::1] n_w, DOUBLE beta, DOUBLE theta,
                            DOUBLE improvement, INT n_iterations):
    '''
    Folding-in using (Tempered) Expectation-Maximization Algorithm for pLSA
    -----------------------------------------------------------------------
    Estimates topic distributions for documents not seen during learning of the model.
    
    The corresponding input topic distributions will be updated (i.e. p_z_d for given range
    of documents), the word distribution per topic is hold fixed.
    
    This implementation is for dense document-term matrices, hence the name. Similar method
    with sparse_ prefix in its name should be used for sparse matrices.

    Parameters
    ----------
    start: integer
      The index of the 1st document to process.

    end: integer
      The index (exclusive) of the last document to process.

    n_w_d: array of doublese, shape = (n_documents, n_words)
      The the dense document-term count matrix.

    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for each document.

    p_w_z: array of doubles, shape = (n_topics, n_words)
      The word distribution for each topic.

    p_z_w_d: array of doubles, shape = (n_words, n_topics)
      The temporary array for posterior topic distribution given
      a word and a document.

    n_w: array of doubles, shape = (n_documents,)
      The length of the documents. Used for faster normalization of P(z|d),
      hence using double to avoid unnecessary conversion.
      
    beta: double
      The 'inverse computational temperature' used for Tempered Expectation
      Maximization algorithm. 
    
    theta: double
      The (multiplicative) 'heating' rate for Tempered-EM algorithm. If set
      to 1.0 and beta is also 1.0, regular EM algorithm is used.
      
    improvement: double
      The minimum improvement in log-likelihood to keep reestimating
      the topic distributions.
    
    n_iterations: integer
      The (total, not per-beta) number of iterations of Tempered-EM algorithm.
    '''
    cdef INT i, d, w, z
    cdef DOUBLE p_w_d, prev_log_likelihood, curr_log_likelihood
    cdef INT n_words = p_w_z.shape[1]
    cdef INT n_topics = p_z_d.shape[1]
    cdef bint heated_and_not_improved = False
    # Releasing GIL to allow multi-threading.
    with nogil:
        # Compute the initial log-likelihood for the documents.
        prev_log_likelihood = _dense_plsa_log_likelihood(start, end, n_w_d, p_z_d, p_w_z)
        # Run (Tempered) Expecation Maximization.
        for i in range(n_iterations):
            # Reestimate P(z|d) for documents in range [start, end).
            for d in range(start, end):
                # Compute (tempered) P(z|w,d) for current d.
                for w in range(n_words):
                    # Contains (tempered) P(w|d) once the next for-loop finishes.
                    p_w_d = 0.0
                    for z in range(n_topics):
                        p_z_w_d[w, z] = pow(p_z_d[d, z] * p_w_z[z, w], beta)
                        p_w_d += p_z_w_d[w, z]
                    # Normalize to get posterior topic distribution P(z|w,d).
                    if p_w_d != 0.0:
                        for z in range(n_topics):
                            p_z_w_d[w, z] /= p_w_d
                # Reestimate P(z|d) for current d.
                for z in range(n_topics):
                    p_z_d[d, z] = 0.0
                    for w in range(n_words):
                        p_z_d[d, z] += n_w_d[d, w] * p_z_w_d[w, z]
                # Normalize p_z_d to get P(z|d).
                for z in range(n_topics):
                    p_z_d[d, z] /= n_w[d]
            # Compute the log-likelihood of the documents after reestimation of P(z|d).
            curr_log_likelihood = _dense_plsa_log_likelihood(start, end, n_w_d, p_z_d, p_w_z)
            # Shoud we heat up or stop?
            if curr_log_likelihood - prev_log_likelihood < improvement:
                # If no heating is wanted (using regular EM), or heating has
                # not led to significant improvement then stop.
                if theta == 1.0 or heated_and_not_improved:
                    break
                else:
                    beta *= theta
                    # Mark that the estimation has just been heated :).
                    heated_and_not_improved = True
            else:
                # Mark the improvement.
                heated_and_not_improved = False


cpdef dense_plsa_log_likelihood(INT start, INT end, INT[:, ::1] n_w_d, DOUBLE[:, ::1] p_z_d,
                                DOUBLE[:, ::1] p_w_z):
    '''
    Log-likelihood for pLSA
    -----------------------
    Computes log-likelihood of the data (dense document-term matrix X) given the pLSA model
    parametrized by p_z_d and p_w_z, i.e. P(z|d) and P(w|z), respectively.
    
    Note that the returned value is not the real log-likelihood but it is off from it
    by a constant, which depends only on the data, so who cares ;).
    
    This implementation is for dense document-term matrices, hence the name. Similar method
    with sparse_ prefix in its name should be used for sparse matrices.

    Parameters
    ----------
    start: integer
      The index of the 1st document to process.

    end: integer
      The index (exclusive) of the last document to process.

    indptr: array of integers
      The index pointer array from the sparse document-term count matrix.

    indices: array of integers
      The indices array from the sparse document-term count matrix.

    counts: array of integers
      The data array from the sparse document-term count matrix.

    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for each document.

    p_w_z: array of doubles, shape = (n_topics, n_words)
      The word distribution for each topic.
    '''
    cdef DOUBLE likelihood
    with nogil:
        likelihood = _dense_plsa_log_likelihood(start, end, n_w_d, p_z_d, p_w_z)
    return likelihood

        
cdef inline DOUBLE _dense_plsa_log_likelihood(INT start, INT end, INT[:, ::1] n_w_d, DOUBLE[:, ::1] p_z_d,
                                              DOUBLE[:, ::1] p_w_z) nogil:
    '''
    The guts of 'dense_plsa_log_likelihood', see above.
    '''
    cdef INT d, w, z
    cdef INT n_words = n_w_d.shape[1], n_topics = p_z_d.shape[1]
    cdef DOUBLE p_w_d, likelihood = 0.0
    # Compute log-likelihood for documents in range [start, end).
    for d in range(start, end):
        for w in range(n_words):
            p_w_d = 0.0
            for z in range(n_topics):
                p_w_d += p_w_z[z, w] * p_z_d[d, z]
            if p_w_d != 0.0:
                likelihood += n_w_d[d, w] * log(p_w_d)
    return likelihood

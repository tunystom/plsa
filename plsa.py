import numpy as np

import pyximport;
pyximport.install(setup_args={"include_dirs":np.get_include()}, reload_support=True)

from joblib import Parallel, delayed, cpu_count

from _plsa import *

from scipy.sparse import issparse, isspmatrix_csr


class PLSA(object):
    ''' 
    Probabilistic Latent Semantic Analysis
    --------------------------------------
    The object implementing pLSA model with methods for learning
    and inference (folding-in unseen documents).
    
    Attributes
    ----------
    p_z_d: array of doubles, shape = (n_documents, n_topics)
      The topic distribution for documents the model was trained on.

    p_w_z: array of doubles, shape = (n_words, n_topics)
      The word distribution for each topic inferred during training.
    
    n_documents: integer
      The number of documents used in learning.
    
    n_words: integer
      The number of words in a topic.
    
    n_topics: integer
      The number of topics.
    '''
    def __init__(self, p_z_d=None, p_w_z=None):
        ''' 
        Initialize the pLSA model. If the topic distributions and word distributions
        are known, they can be provided in 'p_z_d' and 'p_w_z', respectively.

        Parameters
        ----------
        p_z_d: array of doubles, shape = (n_documents, n_topics), or None, default: None
          The topic distribution for each document.

        p_w_z: array of doubles, shape = (n_words, n_topics), or None, default: None
          The word distribution for each topic.
        '''
        _raise = False

        if p_z_d is not None:
            if p_w_z is not None:
                self.p_z_d = p_z_d
                self.p_w_z = p_w_z
                self.n_words = p_w_z.shape[1]
                self.n_topics = p_w_z.shape[0]
                self.n_documents = p_z_d.shape[0]
            else:
                _raise = True
        elif p_w_z is not None:
            _raise = True

        if _raise:
            raise ValueError('both arguments (p_z_d and p_w_z) must be specified,'
                             ' or omitted simultaneously')


    def learn(self, X, heldout=None, n_topics=100, n_iterations=1000, n_folding=1000,
              improvement=1e-3, beta=1.0, theta=1.0, verbose=0, n_jobs=1):
        ''' 
        Learn the parameters of the model from the given document-term
        matrix X. The heldout, if not None, is expected to be an unseen
        document-term matrix (not part of X), which is used to estimate
        the generalization of the model during training and deciding when
        to stop, i.e. early stopping.
        
        Parameters
        ----------
        X: (sparse or dense) array of doubles, shape = (n_documents, n_words)
          The training document-term matrix. If sparse, it is expected
          to be in CSR format.

        heldout: (sparse or dense) array of doubles, or None, shape = (n_documents', n_words')
          The held-out (validation) document-term matrix. If sparse, it is expected
          to be in CSR format.
          
        n_topics: integer, optional, default: 100
          The number of topics that will be learnt with the model.
          
        n_iterations: integer, optional, default: 1000
          The total number of iterations of (Tempered) Expecation-Maximization algorithm.
        
        n_folding:  integer, optional, default: 1000
          The number of iterations of (Tempered) Expecation-Maximization algorithm for folding-in
          of held-out documents. This is used only if heldout is not the same as X.
          
        improvement: double, optional, default: 1e-3
          The minimum improvement in likelihood of the model on held-out data
          that will NOT stop the learning. This parameter is IGNORED if heldout
          is None.
          
        beta: double, optional, default: 1.0
          The 'inverse computational temperature' used for Tempered Expectation
          Maximization algorithm. If set to 1.0, regula EM algorithm is used.
          This parameter has effect only if heldout is NOT None.

        theta: double, optional, default: 1.0
          The (multiplicative) 'heating' rate for Tempered-EM algorithm.
          This parameter has effect only if heldout is NOT None.
          
        verbose: integer, optional, default: 0
          The verbosity level - determines the frequency of printing out progress.
          If 1, print every iteration of EM; if 2, print every 2nd iteration, etc.
          
        n_jobs: integer, optional, default: 1
          The number of worker threads that will be used to run EM algorithm in parallel.
          If negative, the number of workers is determined as #CPU + n_jobs + 1, where
          #CPU is the number of available CPUs.
        '''
        # Check whether the model has not been (partially) trained.
        initialized = hasattr(self, 'p_z_d')

        if initialized:
            # Check the dimensionalities (necessary condition).
            if (self.n_documents, self.n_words) != X.shape:
                raise ValueError('number of parameters of the current model does not'
                                 ' match the dimensionality of the specified data:'
                                 ' perhaps, the model was pretrained on different data')
        else:
            self.n_documents = X.shape[0]
            self.n_words = X.shape[1]
            self.n_topics = n_topics
        
        if n_jobs == 0:
            raise ValueError('n_jobs cannot be 0')
        elif n_jobs < 0:
            n_jobs = cpu_count() + n_jobs + 1 
            
        # If X is sparse we require it to be in CSR format.
        if issparse(X) and not isspmatrix_csr(X):
            X = X.tocsr()
        
        # Subsequent partitioning of documents into segments needs this.
        n_jobs = min(self.n_documents, n_jobs)
        
        # The definition of document segments processed by each worker thread.
        indices = np.linspace(0, self.n_documents, n_jobs + 1).astype('int32')
        
        # Estimated topic distributions for documents.
        if not initialized:
            self.p_z_d  = np.random.rand(self.n_documents, self.n_topics)
            self.p_z_d /= self.p_z_d.sum(axis=1, keepdims=True)
        
        # Document lengths for faster normalization of P(z|d).
        n_w = np.ascontiguousarray(X.sum(axis=1), dtype='float64').ravel()
        
        if np.any(n_w == 0.0):
            raise ValueError('empty documents are not allowed')

        # Estimated word distributions for topics.
        if not initialized:
            self.p_w_z  = np.random.rand(self.n_topics, self.n_words)
            self.p_w_z /= self.p_w_z.sum(axis=1, keepdims=True)
        
        # Estimated (folded-in) topic distributions for heldout documents.
        if heldout is None:
            # Use training data to estimate the log-likelihood.
            heldout = X
        else:
            # If heldout is sparse we require it to be in CSR format.
            if issparse(heldout) and not isspmatrix_csr(X):
                heldout = heldout.tocsr()
            # Used for folding in held-out data.
            p_z_dho  = np.random.rand(heldout.shape[0], self.n_topics)
            p_z_dho /= p_z_dho.sum(axis=1, keepdims=True)

        # Temporary matrices for computation of P(z|w, d) in worker threads.
        p_z_w_d = np.empty((n_jobs, self.n_words, self.n_topics), dtype='float64')

        # Output matrices for partial sum of unnormalized P(w|z) from worker threads.
        p_w_z_out = np.empty((n_jobs, self.n_topics, self.n_words), dtype='float64')

        if heldout is X:
            # Compute the initiale log-likelihood on training data (no folding-in needed).
            prev_heldout_log_likelihood = self._log_likelihood(heldout, self.p_z_d, self.p_w_z, n_jobs=n_jobs)
        else:
            # Compute the initiale log-likelihood on held-out data.
            prev_heldout_log_likelihood = self._heldout_log_likelihood(heldout, p_z_dho, self.p_w_z, p_z_w_d,
                                                                       beta, theta, improvement, n_folding)
        if verbose > 0:
            print 'Initial %s data likelihood: %.4f' % ('training' if heldout is X else 'held-out',
                                                        prev_heldout_log_likelihood)
                
        # Used to determine when to stop after heating up (for TEM).
        heated_and_not_improved = False
        
        # Set the number of iterations (if None).
        n_iterations = 2**30 if n_iterations is None else n_iterations
        
        # Run (Tempered) EM algorithm.
        for iteration in xrange(1, n_iterations + 1):
            # The density of X determines the reestimation function to call.
            if issparse(X):
                # Reestimate the parameters of the model on sparse CSR matrix.
                Parallel(n_jobs=n_jobs, backend='threading')\
                    (delayed(sparse_plsa_update_parameters)(indices[i], indices[i + 1],
                                                            X.indptr, X.indices, X.data,
                                                            self.p_z_d, self.p_w_z,
                                                            p_z_w_d[i], p_w_z_out[i], n_w, beta)
                     for i in range(n_jobs))
            else:
                # Reestimate the parameters of the model on dense matrix.
                Parallel(n_jobs=n_jobs, backend='threading')\
                    (delayed(dense_plsa_update_parameters)(indices[i], indices[i + 1],
                                                           X, self.p_z_d, self.p_w_z,
                                                           p_z_w_d[i], p_w_z_out[i], n_w, beta)
                     for i in range(n_jobs))

            # This need to be done outside of workers.
            self.p_w_z  = p_w_z_out.sum(axis=0)
            self.p_w_z /= self.p_w_z.sum(axis=1, keepdims=True)
            
            if heldout is X:
                # Compute the initiale log-likelihood on training data (no folding-in needed).
                curr_heldout_log_likelihood = self._log_likelihood(heldout, self.p_z_d, self.p_w_z, n_jobs=n_jobs)
            else:
                # Compute the initiale log-likelihood on held-out data.
                curr_heldout_log_likelihood = self._heldout_log_likelihood(heldout, p_z_dho, self.p_w_z, p_z_w_d,
                                                                           beta, theta, improvement, n_folding)
            if (verbose > 0) and ((iteration % verbose) == 0):
                print 'EM iteration %d: %.4f (increase: %.4f).' \
                          % (iteration, curr_heldout_log_likelihood,
                             curr_heldout_log_likelihood - prev_heldout_log_likelihood)

            # If the log-likelihood has not improved significantly, heat up or stop.
            if curr_heldout_log_likelihood - prev_heldout_log_likelihood < improvement:
                # If no heating is wanted (using regular EM), or heating has
                # not led to significant improvement then stop.
                if theta == 1.0 or heated_and_not_improved:
                    break
                else:
                    beta *= theta
                    # Mark that the estimation has just been heated :).
                    heated_and_not_improved = True

                    if verbose > 0:
                        print 'EM iteration %d: heating up -> beta = %.4f' % (iteration, beta)
            else:
                # Mark the improvement.
                heated_and_not_improved = False

            prev_heldout_log_likelihood = curr_heldout_log_likelihood
                
    
    def infer(self, X, n_iterations=1000, improvement=1e-3, beta=1.0, theta=1.0, n_jobs=1):
        ''' 
        Infer the topic distributions for the documents in the given
        document-term matrix X.
        
        Parameters
        ----------
        X: (sparse or dense) array of doubles, shape = (n_documents, n_words)
          The input document-term matrix. If sparse, it is expected
          to be in CSR format.
          
        n_iterations: integer, or None, default: 1000
          The total number of iterations of (Tempered) Expecation-Maximization algorithm.
          If None, the estimation stops when the log-likelihood stops improving.
          
        improvement: double, optional, default: 1e-3
          The minimum improvement in likelihood of the model on held-out data
          that will NOT stop the learning. This parameter is IGNORED if heldout
          is None.
          
        beta: double, optional, default: 1.0
          The 'inverse computational temperature' used for Tempered Expectation
          Maximization algorithm. If set to 1.0, regula EM algorithm is used.
          This parameter has effect only if heldout is NOT None.

        theta: double, optional, default: 1.0
          The (multiplicative) 'heating' rate for Tempered-EM algorithm.
          This parameter has effect only if heldout is NOT None.
          
        n_jobs: integer, optional, default: 1
          The number of worker threads that will be used to run Tempered-EM algorithm
          in parallel. If negative, the number of workers is determined as
          #CPU + n_jobs + 1, where #CPU is the number of available CPUs on the machine.
        '''
        if X.shape[1] != self.n_words:
            raise ValueError('the number of words in input documents != '
                             'number of words in documents the model was trained on')
        if n_jobs == 0:
            raise ValueError('n_jobs cannot by 0')
        elif n_jobs < 0:
            n_jobs = cpu_count() + n_jobs + 1 
        
        # Subsequent partitioning of documents into segments needs this.
        n_jobs = min(X.shape[0], n_jobs)
        
        # The definition of document segments processed by each worker thread.
        indices = np.linspace(0, X.shape[0], n_jobs + 1).astype('int32')
        
        # Set the number of iterations (if None).
        n_iterations = 2**30 if n_iterations is None else n_iterations
        
        # Estimated topic distributions for documents.
        p_z_d  = np.random.rand(X.shape[0], self.n_topics)
        p_z_d /= p_z_d.sum(axis=1, keepdims=True)
        
        # Document lengths for faster normalization of P(z|d).
        n_w = np.ascontiguousarray(X.sum(axis=1), dtype='float64').ravel()
        
        if np.any(n_w == 0.0):
            raise ValueError('empty documents are not allowed')
        
        # Temporary matrices for computation of P(z|w, d) in worker threads.
        p_z_w_d = np.empty((n_jobs, self.n_words, self.n_topics), dtype='float64')
        
        # The density of X determines the reestimation function to call.
        if issparse(X):
            # If X is sparse we require it to be in CSR format.
            if not isspmatrix_csr(X):
                X = X.tocsr()
                
            # Reestimate the parameters of the model on sparse CSR matrix.
            Parallel(n_jobs=n_jobs, backend='threading')\
                (delayed(sparse_plsa_folding_in)(indices[i], indices[i + 1],
                                                 X.indptr, X.indices, X.data,
                                                 p_z_d, self.p_w_z,
                                                 p_z_w_d[i], n_w, beta, theta,
                                                 improvement, n_iterations)
                 for i in range(n_jobs))
        else:
            # Reestimate the parameters of the model on dense matrix.
            Parallel(n_jobs=n_jobs, backend='threading')\
                (delayed(dense_plsa_folding_in)(indices[i], indices[i + 1],
                                                X, p_z_d, self.p_w_z,
                                                p_z_w_d[i], n_w, beta, theta,
                                                improvement, n_iterations)
                 for i in range(n_jobs))
                
        # Returned the estimated topic distribution for each document.
        return p_z_d


    def _log_likelihood(self, X, p_z_d, p_w_z, n_jobs=1):
        ''' 
        Computes a (shifted) log-likelihood of the document-term matrix X
        given the current parameters of the pLSA model.

        The 'real' likelihood is computed as

           \sum_{d}\sum_{w} X[d, w] * log(P(d, w))

        but since the parametrization of the model is using distributions
        P(z|d) and P(w|z), and because of computational reasons, this
        method computes the 'log-likelihood' as follows

           \sum_{d}\sum_{w} X[d, w] * log(P(w|d))

        Note that the difference between the real and shifted likelihood
        is given by

           \sum_{d}\sum_{w} X[d, w] * log(P(d))

        which is trivially maximized by setting

           P(d) = (\sum_{w} X[d, w]) / (\sum_{d}\sum_{w} X[d, w])

        Parameters
        ----------
        X: (sparse or dense) array of doubles, shape = (n_documents, n_words)
          The document-term matrix. If sparse, it is expected
          to be in CSR format.
          
        p_z_d: array of doubles, shape = (n_documents, n_topics)
          The topic distribution for each document.

        p_w_z: array of doubles, shape = (n_topics, n_words)
          The word distribution for each topic.

        n_jobs: integer, optional, default: 1
          The number of worker threads that will be used.
        '''
        # Sanity check for the correctness of the subsequent partitioning.
        n_jobs = min(X.shape[0], n_jobs)
        
        # The definition of document segments processed by each worker thread.
        indices = np.linspace(0, X.shape[0], n_jobs + 1).astype('int32')

        # Call appropriate function based on density of X.
        if issparse(X):
            return np.sum(Parallel(n_jobs=n_jobs, backend='threading')\
                              (delayed(sparse_plsa_log_likelihood)(indices[i], indices[i + 1],
                                                                   X.indptr, X.indices, X.data,
                                                                   p_z_d, p_w_z)
                               for i in range(n_jobs)))
        else:
            return np.sum(Parallel(n_jobs=n_jobs, backend='threading')\
                              (delayed(dense_plsa_log_likelihood)(indices[i], indices[i + 1],
                                                                  X, p_z_d, p_w_z)
                               for i in range(n_jobs)))


    def _heldout_log_likelihood(self, X, p_z_d, p_w_z, p_z_w_d, beta=1.0, theta=1.0,
                                improvement=1e-3, n_iterations=1000):
        ''' 
        Computes a log-likelihood for folded-in documents out of 
        the document-term matrix X given the current parameters
        of the pLSA model.

        Parameters
        ----------
        X: (sparse or dense) array of doubles, shape = (n_documents, n_words)
          The document-term matrix. If sparse, it is expected
          to be in CSR format.
        
        p_z_d: array of doubles, shape = (n_documents, n_topics)
          The output array for inferred topic distribution for each document.

        p_w_z: array of doubles, shape = (n_topics, n_words)
          The word distribution for each topic.
          
        p_z_w_d: array of doubles, shape = (n_words, n_topics)
          The temporary array for posterior topic distribution given
          a word and a document.

        beta: double, optional, default: 1.0
          The 'inverse computational temperature' used for Tempered Expectation
          Maximization algorithm. If set to 1.0, regula EM algorithm is used.

        theta: double, optional, default: 1.0
          The (multiplicative) 'heating' rate for Tempered-EM algorithm.

        improvement: double, optional, default: 1e-3
          The minimum improvement in log-likelihood to keep reestimating
          the topic distributions.

        n_iterations: integer, optional, default: 1000
          The (total, not per-beta) number of iterations of Tempered-EM algorithm.

        n_jobs: integer, optional, default: 1
          The number of worker threads that will be used.
        '''
        # The maximum number of jobs is given by the size of the temporary array.
        # The minimum here is because of the subsequent partitioning.
        n_jobs = min(X.shape[0], p_z_w_d.shape[0])
                
        # The definition of document segments processed by each worker thread.
        indices = np.linspace(0, X.shape[0], n_jobs + 1).astype('int32')
        
        # Call appropriate function based on density of X.
        if issparse(X):
            # Fold-in the given document - estimate P(z|d).
            Parallel(n_jobs=n_jobs, backend='threading')\
                (delayed(sparse_plsa_folding_in)(indices[i], indices[i + 1],
                                                 X.indptr, X.indices, X.data,
                                                 p_z_d, p_w_z, p_z_w_d[i], beta,
                                                 theta, improvement, n_iterations)
                 for i in range(n_jobs))
        else:
            # Fold-in the given document - estimate P(z|d).
            Parallel(n_jobs=n_jobs, backend='threading')\
                (delayed(dense_plsa_folding_in)(indices[i], indices[i + 1], X, p_z_d, p_w_z,
                                                p_z_w_d[i], beta, theta, improvement, n_iterations)
                 for i in range(n_jobs))
                
        # Having folded-in the held-out documents we can proceed with computation of their likelihood.
        return self._log_likelihood(X, p_z_d, p_w_z, n_jobs=n_jobs)

    def print_topics(self, n_words=10, vocabulary=None, precision=4):
        ''' 
        Prints the top most probable words from each topic.

        Parameters
        ----------
        n_words: integer, optional, default: 10
          The number of words to print from each topic.

        vocabulary: array-like of strings or None, optional
          The list of words corresponding to the coordinates
          of the conditional distribution vector P(w|z).

        precision: integer, optional
          The how many decimal places will be shown
          for the probabilities of the words.
        '''
        if vocabulary is None:
            vocabulary = np.array(['%s' % s for s in xrange(self.n_words)], dtype='object')
        else:
            vocabulary = np.array(vocabulary, dtype='object')

        for z, p_w_z in enumerate(self.p_w_z):
            header = 'Topic #%d' % z
            print header
            print '-' * len(header)
            # Select the words from the topic.
            indices = np.argsort(p_w_z)[:-n_words:-1]
            words = vocabulary[indices]
            probs = p_w_z[indices]
            fmt = '%%%ds  (%%.%df)' % (max(map(len, words)),precision)
            for wp in zip(words, probs):
                print fmt % wp
            print


    def save(self, filename):
        ''' 
        Save the model under the specified filename.

        Parameters
        ----------
        filename: string
            The filename where the object is saved.
        '''
        np.savez(filename, p_z_d=self.p_z_d, p_w_z=self.p_w_z)


    @staticmethod
    def load(filename):
        bundle = np.load(filename)
        return PLSA(p_z_d=bundle['p_z_d'], p_w_z=bundle['p_w_z'])
